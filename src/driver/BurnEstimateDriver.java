package driver;

import exercises.*;
import person.Person;

public class BurnEstimateDriver {

	//some tests to make sure program works
	
	public static void main(String[] args) {
		Person p = new Person(135, 65, 63, "Male");
		BoxingInRing bir = new BoxingInRing();
		Hiking hiking = new Hiking();
		Badminton b = new Badminton();
		Walking2 w = new Walking2();
		
		int minutes = 60;
		int result = bir.calculate(minutes, p);
		int result2 = hiking.calculate(minutes, p);
		int result3 = b.calculate(minutes, p);
		int result4 = w.calculate(minutes, p);
		
		System.out.println(result + " calories burned from " + bir.getExerciseName() + " for " + minutes + " minutes.");
		System.out.println(result2 + " calories burned from " + hiking.getExerciseName() + " for " + minutes + " minutes.");
		System.out.println(result3);
		System.out.println(result4);
	}

}
