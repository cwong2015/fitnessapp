package person;

//some physical attributes that affects caloric burn

public class Person {
	public int weight = 0;
	public int height = 0;
	public int age = 0;
	public String gender = "";

	public Person(int myWeight, int myHeight, int myAge, String myGender) {
		weight = myWeight;
		height = myHeight;
		age = myAge;
		gender = myGender;

	}

	public int getWeight() {
		return weight;
	}
	
	public int getHeight(){
		return height;
	}

	public int getAge() {
		return age;
	}

	public String getGender() {
		return gender;
	}


}
