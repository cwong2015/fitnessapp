package exercises;

import interfaces.IBurnEstimate;
import factors.*;
import person.Person;

public class BoxingPB implements IBurnEstimate {

	@Override
	public int calculate(int minutes, Person p) {
		int calorie = (int)(5.5 * minutes);

		OutsideFactors OF = new OutsideFactors(p.getAge(), p.getWeight(), p.getHeight(), calorie, p.getGender());
		calorie = OF.getFactor();

		return calorie;

	}

	@Override
	public String getExerciseName() {
		return "Boxing (punching bag)";
	}

}
