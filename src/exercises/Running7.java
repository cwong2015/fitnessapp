package exercises;

import factors.OutsideFactors;
import interfaces.IBurnEstimate;
import person.Person;

public class Running7 implements IBurnEstimate{

	@Override
	public int calculate(int minutes, Person p) {
		int calorie = 11 * minutes;

		OutsideFactors OF = new OutsideFactors(p.getAge(), p.getWeight(), p.getHeight(), calorie, p.getGender());
		calorie = OF.getFactor();

		return calorie;
	}

	@Override
	public String getExerciseName() {
		return "Running (7 miles/hr)";
	}

}
