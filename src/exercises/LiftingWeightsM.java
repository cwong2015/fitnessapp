package exercises;

import factors.OutsideFactors;
import interfaces.IBurnEstimate;
import person.Person;

public class LiftingWeightsM implements IBurnEstimate{

	@Override
	public int calculate(int minutes, Person p) {
		int calorie = 4 * minutes;

		OutsideFactors OF = new OutsideFactors(p.getAge(), p.getWeight(), p.getHeight(), calorie, p.getGender());
		calorie = OF.getFactor();

		return calorie;
	}

	@Override
	public String getExerciseName() {
		return "Lifting Weights (Moderate)";
	}

}
