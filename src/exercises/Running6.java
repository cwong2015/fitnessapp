package exercises;

import factors.OutsideFactors;
import interfaces.IBurnEstimate;
import person.Person;

public class Running6 implements IBurnEstimate{

	@Override
	public int calculate(int minutes, Person p) {
		int calorie = (int)(9.8 * minutes);

		OutsideFactors OF = new OutsideFactors(p.getAge(), p.getWeight(), p.getHeight(), calorie, p.getGender());
		calorie = OF.getFactor();

		return calorie;

	}

	@Override
	public String getExerciseName() {
		// TODO Auto-generated method stub
		return "Running (6 miles/hr)";
	}

}
