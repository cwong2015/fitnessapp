package guilayer;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;

import javax.swing.*;

import exercises.BoxingInRing;
import interfaces.IBurnEstimate;
import person.Person;

//gui layer

public class View extends JFrame implements ActionListener {

	private JButton add, finish;
	private JTextField agetf, weighttf, heighttf, minutestf, resulttf;
	private JComboBox<String> exercises, gender;
	private JLabel agel, weightl, heightl, genderl, minutesl, resultl;
	private JPanel face, north, south, center, center2, supercenter, centerPanel;
	private ArrayExercises ae = new ArrayExercises();
	private IBurnEstimate[] ex = ae.getExercises();
	private double total = 0;

	public JPanel createContent() {

		// whole panel
		face = new JPanel();
		face.setLayout(new BorderLayout());

		// section panels inside whole panel
		north = new JPanel();
		south = new JPanel();
		center = new JPanel();
		centerPanel = new JPanel();
		center2 = new JPanel();
		supercenter = new JPanel();

		north.setLayout(new FlowLayout());
		south.setLayout(new FlowLayout());
		center.setLayout(new FlowLayout());
		centerPanel.setLayout(new BorderLayout());
		center2.setLayout(new FlowLayout());
		supercenter.setLayout(new FlowLayout());

		// south panel
		add = new JButton("Add");
		add.addActionListener(this);
		add.setActionCommand("ChooseAdd");

		finish = new JButton("Finish");
		finish.addActionListener(this);
		finish.setActionCommand("ChooseFinish");

		resulttf = new JTextField(5);

		resultl = new JLabel("Results:");

		// center panel, center
		agetf = new JTextField(3);
		agetf.setLayout(new FlowLayout());
		weighttf = new JTextField(3);
		weighttf.setLayout(new FlowLayout());
		heighttf = new JTextField(3);
		heighttf.setLayout(new FlowLayout());
		minutestf = new JTextField(3);
		minutestf.setLayout(new FlowLayout());

		gender = new JComboBox<>(new String[] { "Male", "Female" });
		gender.setLayout(new FlowLayout());
		gender.setSelectedIndex(-1);
		gender.addActionListener(this);

		// supercenter panel
		exercises = new JComboBox<>(new String[0]);
		constructExerciseOptions();
		exercises.setLayout(new FlowLayout(FlowLayout.CENTER));
		exercises.setEditable(true);
		exercises.setSelectedIndex(-1);
		exercises.addActionListener(this);

		// north panel, center
		agel = new JLabel("Age:");
		weightl = new JLabel("Weight:");
		heightl = new JLabel("Height:");
		genderl = new JLabel("Gender:");
		minutesl = new JLabel("Minutes:");

		// and go
		south.add(add);
		south.add(finish);
		south.add(resultl);
		south.add(resulttf);

		center2.add(agetf);
		center2.add(weighttf);
		center2.add(heighttf);
		center2.add(gender);
		center2.add(minutestf);
		supercenter.add(exercises, BorderLayout.CENTER);
		centerPanel.add(supercenter, BorderLayout.SOUTH);
		centerPanel.add(center2, BorderLayout.NORTH);

		center.add(centerPanel);

		north.add(agel);
		north.add(weightl);
		north.add(heightl);
		north.add(genderl);
		north.add(minutesl);

		face.add(north, BorderLayout.NORTH);
		face.add(south, BorderLayout.SOUTH);
		face.add(center, BorderLayout.CENTER);

		return face;

	}

	private void constructExerciseOptions() {
		for (int i = 0; i < ex.length; i++) {
			exercises.addItem(ex[i].getExerciseName());
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("ChooseAdd")) {
			try {
				total += retrieveInfoAndUse();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if(e.getActionCommand().equals("ChooseFinish")){
			try {
				resulttf.setText("" + (int)total + " cal");
				System.out.println(total);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if (e.getSource().equals(add)) {
			System.out.println("Added!");
		}
		

	}

	private double retrieveInfoAndUse() throws Exception {
		JComboBox<String> combo = exercises;
		String selectedChoice = (String) combo.getSelectedItem();

		int age = Integer.parseInt(agetf.getText());
		int weight = Integer.parseInt(weighttf.getText());
		int height = Integer.parseInt(heighttf.getText());
		String gender = ((String) this.gender.getSelectedItem());
		int minutes = Integer.parseInt((String) minutestf.getText());
		String result = "";

		Person p = new Person(weight, height, age, gender);

		for (int i = 0; i < ex.length; i++) {
			if (selectedChoice.equals((combo).getItemAt(i))) {

				try {
					result = "" + ex[i].calculate(minutes, p);

				} catch (Exception ex) {
					throw ex;
				}

			}
		}
		resulttf.setText(result + " cal");
		return Double.parseDouble(result);

	}

}
