package guilayer;

import java.awt.Color;

import javax.swing.JFrame;

public class Frame {

	public static void createFrame() {
		JFrame frame = new JFrame("Fitness App");
		frame.setVisible(true);

		View view = new View();
		frame.setContentPane(view.createContent());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(360, 640);
		frame.getContentPane().setBackground(Color.lightGray);
	}
}
