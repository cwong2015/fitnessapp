package guilayer;

import exercises.*;
import interfaces.IBurnEstimate;

public class ArrayExercises {
	
	public IBurnEstimate[] getExercises (){
		IBurnEstimate[] ex = {
				new Badminton(),
				new Basketball(),
				new BoxingInRing(),
				new BoxingPB(),
				new CalisthenicsL(),
				new CalisthenicsM(),
				new CalisthenicsV(),
				new Football(),
				new Handball(),
				new Hiking(),
				new Jogging(),
				new JumpRopeF(),
				new JumpRopeM(),
				new JumpRopeS(),
				new LiftingWeightsM(),
				new LiftingWeightsV(),
				new MountainClimbing(),
				new PingPong(),
				new Running10(),
				new Running5(),
				new Running6(),
				new Running7(),
				new Running8(),
				new Running9(),
				new Skateboarding(),
				new SoccerC(),
				new Stretching(),
				new SwimmingG(),
				new SwimmingV(),
				new TennisS(),
				new Walking2(),
				new Walking3(),
				new Walking4()
		};
		return ex;

	}

}
