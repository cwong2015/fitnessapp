package factors;

//outside factors weight in

//Men: BMR = 66 + ( 6.23 x weight in pounds ) + ( 12.7 x height in inches ) - ( 6.8 x age in year )
//Women: BMR = 655 + ( 4.35 x weight in pounds ) + ( 4.7 x height in inches ) - ( 4.7 x age in years )

//Calorie Burn = (BMR / 24) x MET x T 

public class OutsideFactors {
	private double age, weight, height, caloriesBurned;
	private String gender;

	public OutsideFactors(double theAge, double theWeight, double theHeight, double theCaloriesBurned,
			String theGender) {
		age = theAge;
		weight = theWeight;
		height = theHeight;
		caloriesBurned = theCaloriesBurned;
		gender = theGender;
	}

	public int getFactor() {
		double caloriesBurnedAfterFactors;

		double mBMR = 66 + (6.23 * weight) + (12.7 * height) - (6.8 * age);
		double fBMR = 655 + (4.35 * weight) + (4.7 * height) - (4.7 * age);
		double BMRScale;
		double multiplier;
		
		if(gender.equals("Male")){
			BMRScale = (mBMR)/24;
			multiplier = BMRScale * caloriesBurned;
			caloriesBurnedAfterFactors = multiplier/60;
					
			return (int)(caloriesBurnedAfterFactors);
		}
			
			BMRScale = (fBMR)/24;
			multiplier = BMRScale * caloriesBurned;
			caloriesBurnedAfterFactors = multiplier/60;
			
			return (int)(caloriesBurnedAfterFactors);
		

	}

}
